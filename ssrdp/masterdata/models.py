from __future__ import unicode_literals
from django.db import models
import constants as C
from django.contrib.auth import get_user_model


class BaseContent(models.Model):
    ACTIVE_CHOICES = ((0, 'Inactive'), (2, 'Active'),)
    active = models.PositiveIntegerField(choices=ACTIVE_CHOICES,
                                         default=2)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    created_by = models.ForeignKey('usermanagement.UserProfile',
                                   blank=True, null=True, related_name='+')
    modified_by = models.ForeignKey('usermanagement.UserProfile',
                                    blank=True, null=True, related_name='+')

    def __str__(self):
        for i in C.FACE:
            if hasattr(self, i):
                return str(getattr(self, i))
        return super(BaseContent, self).__str__()

    class Meta:
        abstract = True

    def switch(self):
        self.active = {2: 0, 0: 2}[self.active]
        self.save()


class Country(BaseContent):
    name = models.CharField(max_length=100)
    class Admin:
        inlines = ['State']


class State(BaseContent):
    country = models.ForeignKey('Country')
    name = models.CharField(max_length=100)
    class Admin:
        inlines = ['District']

    @staticmethod
    def filter_queryset_for_user(qs, user):
        if user.role == 'admin':
            return qs
        elif user.role in ['co', 'p']:
            try:
                co = user.coordinator
            except:
                co = user.participant
            districts = District.filter_queryset_for_user(District.objects.all(), user)
            return qs.filter(id__in=districts.values_list('state', flat=True))


class District(BaseContent):
    state = models.ForeignKey('State')
    name = models.CharField(max_length=100)

    @staticmethod
    def filter_queryset_for_user(qs, user):
        if user.role == 'admin':
            return qs
        elif user.role in ['co', 'p']:
            try:
                co = user.coordinator
            except:
                co = user.participant
            return qs.filter(id__in=co.district.values_list('id', flat=True))


class Taluk(BaseContent):
    district = models.ForeignKey('District')
    name = models.CharField(max_length=100)


class Village(BaseContent):
    taluk = models.ForeignKey('Taluk')
    name = models.CharField(max_length=100)
