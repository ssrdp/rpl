from crud.admin import register
from .models import Country, State, District
from usermanagement.models import UserProfile, Coordinator, Participant
from course.models import *
from survey.models import *
namespace = globals()


for i in [
        Country, State, District, UserProfile, Coordinator, Participant,
        SkillDevelopmentSector, JobRole, Quota, Question, Batch, Student,
        Survey, YogaShivir, NadiCamp,
]: register(i, namespace)
