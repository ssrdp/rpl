def is_admin(user):
    try:
        return user.role == 'admin'
    except:
        return False


def truth_func(user):
    return True


CONF = {
    'menu': [
        {'name': 'Home', 'url': '/'},

        {'name': 'Coordinator', 'permission': is_admin, 'submenu': [
            {'name': 'List Coordinator', 'url': '/user/coordinator/'},
            {'name': 'Add Coordinator', 'url': '/user/coordinator/add/'},
        ]},

        {'name': 'Batch', 'permission': is_admin, 'submenu': [
            {'name': 'List Batch', 'url': '/batch/'},
            {'name': 'Add Batch', 'url': '/batch/add/'},
        ]},

        {'name': 'Configure', 'permission': is_admin, 'submenu': [
            # {'name': 'State', 'url': '/state/'},
            {'name': 'Coordinator', 'url': '/user/coordinator/'},
            {'name': 'Skill Development Sector', 'url': '/skilldevelopmentsector/'},
            {'name': 'Quota', 'url': '/quota/'},
            # {'name': 'Quota', 'url': '/skilldevelopmentsector/?alert=Click on Manage job role for desired skill development sector and then click manage quota on desired job role'},
        ]},
    ],
    'model_map': {
        'state': {
            'address': 'masterdata.state',
            'buttons': [{'name': 'Manage Districts', 'url': '/district/state-%s/'}],
            'fields': 'code name',
            'permission': is_admin,
        },
        'district': {
            'address': 'masterdata.district',
            'fields': 'state code name',
            'permission': is_admin,
            'breadcrum': (
                {'title': 'Home', 'link': '/'},
                {'title': 'States', 'link': '/state/'},
                {'title_from': ''},
            ),
        },
        'coordinator': {
            'address': 'usermanagement.Coordinator',
            'fields': 'first_name last_name username email districts job_roles sectors',
            'add_fields': 'username password first_name last_name email district job_role',
            'edit_fields': 'username first_name last_name email district job_role',
            'permission': is_admin,
        },
        'skilldevelopmentsector': {
            'address': 'course.SkillDevelopmentSector',
            'nicely': 'Skill Development Sector',
            'fields': 'sl_no name',
            'permission': is_admin,
            'buttons': [{'name': 'Manage Job Role', 'url': '/jobrole/skill_development_sector-%s/'}],
        },
        'jobrole': {
            'address': 'course.JobRole',
            'fields': 'sl_no skill_development_sector name',
            'permission': is_admin,
            'buttons': [
                {'name': 'Manage Quota', 'url': '/quota/job_role-%s/'},
                {'name': 'Manage Questions', 'url': '/question/job_role-%s/'},
            ],
            'breadcrum': (
                {'title': 'Home', 'link': '/'},
                {
                    'title': 'Skill development sectors',
                    'link': '/skilldevelopmentsector/',
                },
                {'title_from': ''},
            ),
        },
        'quota': {
            'address': 'course.Quota',
            'fields': 'sl_no job_role state quota',
            'permission': is_admin,
            'buttons': [
                {'name': 'Manage Batch', 'url': '/batch/quota-%s/'},
            ],
            'breadcrum': (
                {'title': 'Home', 'link': '/'},
                {
                    'title': 'Skill development sectors',
                    'link': '/skilldevelopmentsector/',
                },
                {
                    'title_from': 'skill_development_sector',
                    'link': '/jobrole/skill_development_sector-%s/',
                    'link_format': 'skill_development_sector.id',
                },
                {'title_from': ''},
            ),
        },
        'batch': {
            'address': 'course.Batch',
            'fields': 'district coordinator',
            'permission': is_admin,
            'breadcrum': (
                {'title': 'Home', 'link': '/'},
                {
                    'title': 'Skill development sectors',
                    'link': '/skilldevelopmentsector/',
                },
                {
                    'title_from': 'job_role.skill_development_sector',
                    'link': '/jobrole/skill_development_sector-%s/',
                    'link_format': 'job_role.skill_development_sector.id',
                },
                {
                    'title_from': 'job_role',
                    'link': '/quota/job_role-%s/',
                    'link_format': 'job_role.id',
                },
                {'title_from': ''},
            ),
        },
        'question': {
            'address': 'course.Question',
            'fields': ('question option_a option_b option_c option_d answer'
                       'question_hindi option_a_hindi option_b_hindi'
                       'option_c_hindi option_d_hindi', ),
            'permission': is_admin,
            'breadcrum': (
                {'title': 'Home', 'link': '/'},
                {
                    'title': 'Skill development sectors',
                    'link': '/skilldevelopmentsector/',
                },
                {
                    'title_from': 'skill_development_sector',
                    'link': '/jobrole/skill_development_sector-%s/',
                    'link_format': 'skill_development_sector.id',
                },
                {'title_from': ''},
            ),
        },
    },
}
