from course.course_conf import form1, form3
from survey.models import *


def is_admin(user):
    try:
        return user.role == 'admin'
    except:
        return False


def is_part(user):
    try:
        return user.role == 'p'
    except:
        return False


def admin_or_cordinator(user):
    try:
        return user.role in ['admin', 'co']
    except:
        return False

def logged_in(user):
    return user.is_authenticated()


def not_logged_in(user):
    return not user.is_authenticated()


def truth_func(user):
    return True


CONF = {
    'menu': [
	{'name': 'Candidate Registration', 'url': '/course/Student/add/', 'permission': not_logged_in},
	{'name': 'Trainer Enrollment', 'url': '/course/Teacher/add/', 'permission': not_logged_in},
	{'name': 'Quota and Courses', 'url': '/Batch/', 'permission': not_logged_in},
	{'name': 'SSRDP', 'url': 'https://ssrdp.org/', 'permission': not_logged_in},

        {'name': 'Home', 'url': '/', 'permission': admin_or_cordinator},
        {'name': 'Manage Locations', 'url': '/state/', 'permission': is_admin},
        
        {'name': 'Users', 'permission': is_admin, 'submenu': [
            {'name': 'List Coordinator', 'url': '/user/coordinator/'},
            {'name': 'Add Coordinator', 'url': '/user/coordinator/add/'},
            {'name': 'List Participant', 'url': '/user/participant/'},
            {'name': 'Add Participant', 'url': '/user/participant/add/'},
        ]},

        
        {'name': 'Forms', 'submenu': [
            {'name': 'Survey', 'url': '/survey/'},
            {'name': 'Yoga Shivir', 'url': '/YogaShivir/'},
            {'name': 'Nadi Camp', 'url': '/NadiCamp/'},
        ]},

	{'name': 'List Panchayath Representative', 'url': '/pr/', 'permission': admin_or_cordinator},
        {'name': 'Panchayath Representative', 'permission': is_part, 'submenu': [
            {'name': 'List Panchayath Representative', 'url': '/pr/'},
            {'name': 'Add Panchayath Representative', 'url': '/pr/add/'},
        ]},

        {'name': 'Batch', 'permission': admin_or_cordinator, 'submenu': [
            {'name': 'List Batch', 'url': '/batch/'},
            {'name': 'Add Batch', 'url': '/batch/add/'},
        ]},

        {'name': 'Skill Development Sector', 'url': '/skilldevelopmentsector/', 'permission': is_admin},
        {'name': 'Quota', 'permission': is_admin, 'submenu': [
            {'name': 'List Quota', 'url': '/quota/'},
            {'name': 'Add Quota', 'url': '/quota/add/'},
        ]},
        {'name': 'Q&A', 'permission': is_admin, 'submenu': [
            {'name': 'List Q&A', 'url': '/Question/'},
            {'name': 'Add Q&A', 'url': '/Question/add/'},
        ]},
        
        {'name': 'Students', 'permission': logged_in, 'submenu': [
            {'name': 'List Students', 'url': '/student/'},
            {'name': 'Add Students', 'url': '/student/add/', 'permission': logged_in},
        ]},
        {'name': 'Teachers', 'permission': is_admin, 'submenu': [
            {'name': 'List Teachers', 'url': '/teacher/'},
            {'name': 'Add Teachers', 'url': '/teacher/add/', 'permission': is_admin},
        ]},
    ],
    'model_map': {
        'state': {
            'address': 'masterdata.state',
            'buttons': [{'name': 'Manage Districts', 'url': '/district/state-%s/'}],
            'fields': 'name',
            'permission': is_admin,
        },
        'district': {
            'address': 'masterdata.district',
            'buttons': [{'name': 'Manage Taluk', 'url': '/taluk/district-%s/'}],
            'fields': 'state name',
            'permission': is_admin,
            'breadcrum': (
                {'title': 'Home', 'link': '/'},
                {'title': 'States', 'link': '/state/'},
                {'title_from': ''},
            ),
        },
        'taluk': {
            'address': 'masterdata.Taluk',
            'buttons': [{'name': 'Manage Village', 'url': '/village/taluk-%s/'}],
            'fields': 'district name',
            'permission': is_admin,
            'breadcrum': (
                {'title': 'Home', 'link': '/'},
                {'title': 'States', 'link': '/state/'},
                {
                    'title_from': 'state',
                    'link': '/district/state-%s/',
                    'link_format': 'state.id',
                },
                {'title_from': ''},
            ),
        },
        'village': {
            'address': 'masterdata.Village',
            'fields': 'taluk name',
            'permission': is_admin,
            'breadcrum': (
                {'title': 'Home', 'link': '/'},
                {'title': 'States', 'link': '/state/'},
                {
                    'title_from': 'district.state',
                    'link': '/taluk/district-%s/',
                    'link_format': 'district.id',
                },
                {
                    'title_from': 'district',
                    'link': '/taluk/district-%s/',
                    'link_format': 'district.id',
                },
                {'title_from': ''},
            ),
        },
        'coordinator': {
            'address': 'usermanagement.Coordinator',
            'fields': 'first_name last_name username email districts job_roles sectors',
            'add_fields': 'username password first_name last_name email district job_role',
            'edit_fields': 'username password first_name last_name email district job_role',
            'permission': is_admin,
        },
        'participant': {
            'address': 'usermanagement.Participant',
            'fields': 'coordinator first_name last_name username email districts job_roles sectors aadhar_number adhaar_image',
            'add_fields': 'coordinator username password first_name last_name email district job_role aadhar_number adhaar_image',
            'edit_fields': 'coordinator username password first_name last_name email district job_role aadhar_number adhaar_image',
            'permission': is_admin,
        },
        'pr': {
            'address': 'usermanagement.PanchayathRepresentative',
            'dropdown_wrt': ['district state state',
                             'taluk district district',
                             'village taluk taluk',
            ],
            'datepickers': 'date_of_birth',
            'nicely': 'Panchayath Representative'
        },
        'skilldevelopmentsector': {
            'address': 'course.SkillDevelopmentSector',
            'nicely': 'Skill Development Sector',
            'fields': 'name',
            'permission': is_admin,
            'buttons': [{'name': 'Manage Job Role', 'url': '/jobrole/skill_development_sector-%s/'}],
        },
        'jobrole': {
            'address': 'course.JobRole',
            'fields': 'skill_development_sector name',
            'permission': is_admin,
            'buttons': [
                {'name': 'Manage Quota', 'url': '/quota/job_role-%s/'},
                {'name': 'Manage Questions', 'url': '/question/job_role-%s/'},
            ],
            'breadcrum': (
                {'title': 'Home', 'link': '/'},
                {
                    'title': 'Skill development sectors',
                    'link': '/skilldevelopmentsector/',
                },
                {'title_from': ''},
            ),
        },
        'quota': {
            'address': 'course.Quota',
            'fields': 'job_role state quota',
            'permission': is_admin,
            'model_methods_to_list': 'total_students',
            'breadcrum': (
                {'title': 'Home', 'link': '/'},
                {
                    'title': 'Skill development sectors',
                    'link': '/skilldevelopmentsector/',
                },
                {
                    'title_from': 'skill_development_sector',
                    'link': '/jobrole/skill_development_sector-%s/',
                    'link_format': 'skill_development_sector.id',
                },
                {'title_from': ''},
            ),
        },
        'batch': {
            'address': 'course.Batch',
            'permission': admin_or_cordinator,
            'datepickers': 'course_start_date course_end_date',
            'timepickers': 'course_start_timing course_end_timing',
            'dropdown_wrt': ['district state state',
                             'quota state state',
                             'taluk district district',
                             'village taluk taluk',
                             # 'job_role skill_development_sector sector'
            ],
            'add_fields_exclude': 'job_role sector',
            'buttons': [
                {'name': 'List Students', 'url': '/student/batch-%s/'},
            ],
            'enable_edit': is_admin,
        },
        'Batch': {
            'address': 'course.Batch',
            'permission': not_logged_in,
            'datepickers': 'course_start_date course_end_date',
            'timepickers': 'course_start_timing course_end_timing',
            'dropdown_wrt': ['district state state',
                             'quota state state',
                             'job_role skill_development_sector sector'],
            'enable_edit': is_admin,
        },
        'question': {
            'address': 'course.Question',
            'fields': ('question option_a option_b option_c option_d answer '
                       'hindi_workaround question_hindi option_a_hindi option_b_hindi '
                       'option_c_hindi option_d_hindi'),
            'permission': is_admin,
            'breadcrum': (
                {'title': 'Home', 'link': '/'},
                {
                    'title': 'Skill development sectors',
                    'link': '/skilldevelopmentsector/',
                },
                {
                    'title_from': 'skill_development_sector',
                    'link': '/jobrole/skill_development_sector-%s/',
                    'link_format': 'skill_development_sector.id',
                },
                {'title_from': ''},
            ),
        },

        'Question': {
            'address': 'course.Question',
            'fields': ('job_role question option_a option_b option_c option_d answer'
                       'question_hindi option_a_hindi option_b_hindi'
                       'option_c_hindi option_d_hindi', ),
            'permission': is_admin,
        },

        'student': {
            'address': 'course.Student',
            'permission': logged_in,
            'dropdown_wrt': ['job_role skill_development_sector skill_development_sector',
                             'per_district state per_state',
                             'pre_district state pre_state',
                             'batch job_role job_role',
            ],
            'datepickers': 'application_date date_of_birth',
            'fields': '%s %s' % (form1, form3),
            'add_fields_exclude': 'application_status application_date pf_status',
            'breadcrum': (
                {'title': 'Home', 'link': '/'},
                {
                    'title': 'Batch',
                    'link': '/batch/',
                },
                {'title_from': ''},
            ),
            'buttons': [
                {'name': 'Download pdf', 'url': '/course/as_pdf/%s/'},
            ],
            'enable_delete': is_admin,
        },
        'teacher': {
            'address': 'course.Teacher',
            'permission': is_admin,
            'dropdown_wrt': ['job_role skill_development_sector skill_development_sector',
                             'district state state',
            ],
            'datepickers': 'tot_batch_date',
        },
        'Student': {
            'address': 'course.Student',
            'dropdown_wrt': ['job_role skill_development_sector skill_development_sector',
                             'per_district state per_state',
                             'pre_district state pre_state',
                             'batch job_role job_role',
            ],
            'datepickers': 'application_date date_of_birth',
            'fields': form1,
            'add_fields_exclude': 'application_status pf_status',
            'nicely': 'Candidate Registration',

        },
        'form3': {
            'address': 'course.Student',
            'fields': form3,
        },
        'Teacher': {
            'address': 'course.Teacher',
            'dropdown_wrt': ['job_role skill_development_sector skill_development_sector',
                             'district state state',
            ],
            'datepickers': 'tot_batch_date',
            'nicely': 'Trainer Registration',
        },
        'survey': {
            'address': 'survey.Survey',
            'datepickers': 'dob batch_start_date batch_end_date',
        },
        'YogaShivir': {
            'address': 'survey.YogaShivir',
            'datepickers': 'dob batch_start_date batch_end_date',
            'nicely': 'Yoga Shivir',
        },
        'NadiCamp': {
            'address': 'survey.NadiCamp',
            'datepickers': 'dob batch_start_date batch_end_date',
            'nicely': 'Nadi Camp',
        },
    },
}
