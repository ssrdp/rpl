from __future__ import unicode_literals
from django.db import models
from masterdata.models import BaseContent
import datetime
import uuid


GENDER_CHOICES = (('0', 'Male'), ('1', 'Female'), ('2', 'Other'))


def get_upload_to(model, task):
    def func(self, filename):
        return '%s/%s/%s-%s' % (model, task, self.id, filename)
    return func


func = None


def get_photo_path(self, filename):
    return 'teacher/%s-%s' % (uuid.uuid4(), filename)


class SkillDevelopmentSector(BaseContent):
    name = models.CharField(max_length=100)

    @staticmethod
    def filter_queryset_for_user(qs, user):
        if user.role == 'admin':
            return qs
        elif user.role in ['co', 'p']:
            try:
                co = user.coordinator
            except:
                co = user.participant
            job_roles = JobRole.filter_queryset_for_user(JobRole.objects.all(), user)
            return qs.filter(id__in=job_roles.values_list('skill_development_sector', flat=True))


class JobRole(BaseContent):
    skill_development_sector = models.ForeignKey('SkillDevelopmentSector')
    name = models.CharField(max_length=100)
    class Admin:
        inlines = ["Question", ]

    @staticmethod
    def filter_queryset_for_user(qs, user):
        if user.role == 'admin':
            return qs
        elif user.role in ['co', 'p']:
            try:
                co = user.coordinator
            except:
                co = user.participant
            return qs.filter(id__in=co.job_role.values_list('id', flat=True))


class Quota(BaseContent):
    state = models.ForeignKey('masterdata.State')
    sector = models.ForeignKey('SkillDevelopmentSector')
    job_role = models.ForeignKey('JobRole')
    quota = models.PositiveIntegerField()

    def save(self, *args, **kwargs):
        self.sector = self.job_role.skill_development_sector
        return super(Quota, self).save(*args, **kwargs)

    def total_students(self):
        return Student.objects.filter(batch__quota=self).count()

    def __str__(self):
        return '%s @ %s (%s)' % (
            str(self.job_role), str(self.state), str(self.quota))


class Question(BaseContent):
    ANSWER_CHOICES = (('A', 'A'), ('B', 'B'), ('C', 'C'), ('D', 'D'))
    job_role = models.ForeignKey('JobRole')

    question = models.CharField(max_length=200)
    option_a = models.CharField(max_length=50)
    option_b = models.CharField(max_length=50)
    option_c = models.CharField(max_length=50)
    option_d = models.CharField(max_length=50)
    answer = models.CharField(choices=ANSWER_CHOICES,
                              max_length=10)

    hindi_workaround = models.CharField(max_length=300, blank=True, null=True)

    question_hindi = models.CharField(max_length=100, blank=True, null=True)
    option_a_hindi = models.CharField(max_length=50, blank=True, null=True)
    option_b_hindi = models.CharField(max_length=50, blank=True, null=True)
    option_c_hindi = models.CharField(max_length=50, blank=True, null=True)
    option_d_hindi = models.CharField(max_length=50, blank=True, null=True)

    def save(self, *args, **kwargs):
        if self.hindi_workaround:
            w = self.hindi_workaround.split('\t')
            w = w + [None] * (5 - len(w))
            self.hindi_workaround = None
            (self.question_hindi, self.option_a_hindi, self.option_b_hindi,
             self.option_c_hindi, self.option_d_hindi) = w
        return super(Question, self).save(*args, **kwargs)

    def __str__(self):
        return self.question

    class Admin:
        list_filter = ('job_role', 'job_role__skill_development_sector')


class Batch(BaseContent):
    batch_capacity = models.PositiveIntegerField()
    course_start_date = models.DateField()
    course_end_date = models.DateField()
    course_start_timing = models.CharField(max_length=40)
    course_end_timing = models.CharField(max_length=40)

    trainer_name = models.CharField(max_length=100)
    trainer_contact_number = models.CharField(max_length=100)
    trainer_email = models.CharField(max_length=100)

    organizer_name = models.CharField(max_length=100)
    any_volunteer_name = models.CharField(max_length=100)
    contact_number_for_emergency = models.CharField(max_length=100)

    sector = models.ForeignKey('SkillDevelopmentSector')
    job_role = models.ForeignKey('JobRole')

    state = models.ForeignKey('masterdata.State')
    district = models.ForeignKey('masterdata.District')
    taluk = models.ForeignKey('masterdata.Taluk', blank=True, null=True)
    village = models.ForeignKey('masterdata.Village', blank=True, null=True)
    quota = models.ForeignKey('Quota')

    coordinator = models.ForeignKey('usermanagement.Coordinator', blank=True, null=True)

    def clean(self):
        self.job_role = self.quota.job_role
        self.sector = self.job_role.skill_development_sector

    def total_students(self):
        return Student.objects.filter(batch=self).count()

    def __str__(self):
        return '%s - %s (%s at %s, %s)' % (
            self.course_start_date, self.course_end_date,
            self.trainer_name, str(self.district), str(self.state)
        )

    @staticmethod
    def filter_queryset_for_user(qs, user):
        if user.role == 'admin':
            return qs
        elif not user.is_authenticated():
            return qs
        elif user.role in ['co', 'p']:
            try:
                co = user.coordinator
            except:
                co = user.participant
            return qs.filter(district__in=co.district.all(),
                             job_role__in=co.job_role.all())


class Student(BaseContent):
    AFFILIATED_CHOICES = (('0', 'No'), ('1', 'Yes'))
    APPLICATION_CHOICES = (
        ('0', 'Added by admin'),
        ('1', 'Applied directly'),
        ('2', 'Passed test'),
        ('3', 'Failed Test'),
    )
    PASS_FAIL_CHOICES = (
        ('0', 'Result pending'),
        ('1', 'Passed'),
        ('2', 'Failed'),
    )
    NADI_CHOICES = (
        ('0', 'Not Applicable'),
        ('1', 'Not Done'),
        ('2', 'Done'),
    )

    application_status = models.CharField(choices=APPLICATION_CHOICES, max_length=10, default='0')
    pf_status = models.CharField(choices=PASS_FAIL_CHOICES, max_length=10, default='0', verbose_name='Pass or fail')

    are_you_already_affiliated_with_art_of_living = models.CharField(
        choices=AFFILIATED_CHOICES, max_length=10)

    skill_development_sector = models.ForeignKey('SkillDevelopmentSector')
    job_role = models.ForeignKey('JobRole')
    batch = models.ForeignKey('Batch')
    # coordinator = models.ForeignKey('usermanagement.Coordinator')
    nadi_pariksha_is_done = models.CharField(choices=NADI_CHOICES, max_length=10, default='0')
    name_as_per_aadhar_card = models.CharField(max_length=100)

    # state = models.ForeignKey('masterdata.State')
    # district = models.ForeignKey('masterdata.District')
    application_date = models.DateField(default=datetime.datetime.today)
    email = models.CharField(max_length=100)

    fh_name = models.CharField(max_length=100, verbose_name="Father's or Husband's name")
    m_name = models.CharField(max_length=100, verbose_name="Mother's name")

    date_of_birth = models.DateField()
    mobile_number = models.CharField(max_length=100)
    gender = models.CharField(
        choices=GENDER_CHOICES, max_length=10)
    education = models.CharField(max_length=100)
    religion = models.CharField(max_length=100)
    caste = models.CharField(max_length=100)

    permanent_address = models.TextField()
    per_city = models.CharField(max_length=100, verbose_name="City")
    per_state = models.ForeignKey('masterdata.State', related_name='+', verbose_name="State")
    per_district = models.ForeignKey('masterdata.District', related_name='+', verbose_name="District")
    per_constituency = models.CharField(max_length=100, verbose_name="Constituency")
    per_pincode = models.PositiveIntegerField(verbose_name="Pincode")

    same_pre_per = models.BooleanField(default=False,
        verbose_name='Present address is same as permanent_address')

    present_address = models.TextField()
    pre_city = models.CharField(max_length=100, verbose_name="City")
    pre_state = models.ForeignKey('masterdata.State', related_name='+', verbose_name="State")
    pre_district = models.ForeignKey('masterdata.District', related_name='+', verbose_name="District")
    pre_constituency = models.CharField(max_length=100, verbose_name="Constituency")
    pre_pincode = models.PositiveIntegerField(verbose_name="Pincode")

    image = models.ImageField(upload_to=get_upload_to('student', 'image'),null=True,
                              verbose_name="Upload recent photo (below 500 kb)")
    adhaar_image = models.ImageField(upload_to=get_upload_to('student', 'adhaar_image'),null=True,
                              verbose_name="Upload aadhar proof (below 500 kb)")
    edu_cert_image = models.ImageField(upload_to=get_upload_to('student', 'edu_cert_image'),null=True,
                              verbose_name="Upload latest education certificate (below 500 kb)")

    other_remarks = models.TextField(null=True)

    intro_video = models.CharField(max_length=100, null=True,
                             verbose_name=("Link to short intro video (Youtube/Facebook)"))

    work_video = models.CharField(
        max_length=100, null=True, verbose_name=(
        "Link to 1 min video (job role related)"))

    pan_card_number = models.CharField(max_length=100, null=True)
    occupation = models.CharField(max_length=100, null=True)
    # Upload 3 to 5 jobrole related photos

    experiance = models.CharField(max_length=100, null=True)
    job_role_remarks = models.TextField(null=True)
    other_certificates = models.FileField(upload_to='x',
                                          null=True, blank=True)
    other_certificates_1 = models.FileField(upload_to='x', verbose_name='',
                                          null=True, blank=True)
    other_certificates_2 = models.FileField(upload_to='x', verbose_name='',
                                          null=True, blank=True)
    other_certificates_3 = models.FileField(upload_to='x', verbose_name='',
                                          null=True, blank=True)
    other_certificates_4 = models.FileField(upload_to='x', verbose_name='',
                                          null=True, blank=True)


    name_as_per_bank_records = models.CharField(max_length=100, null=True)
    bank_name = models.CharField(max_length=100, null=True)
    branch_name = models.CharField(max_length=100, null=True)
    account_number = models.CharField(max_length=100, null=True)
    ifsc_code = models.CharField(max_length=100, null=True)

    employment_status = models.CharField(max_length=100, null=True)
    acceptance = models.BooleanField(default=False)

    def __str__(self):
        return self.name_as_per_aadhar_card

    @staticmethod
    def filter_queryset_for_user(qs, user):
        if user.role == 'admin':
            return qs
        elif user.role == 'co':
            co = user.coordinator
            batchs = Batch.objects.filter(district__in=co.district.all())
            return qs.filter(
                batch__in=batchs,
                job_role__in=co.job_role.all())
        elif user.role == 'p':
            return qs.filter(created_by=user)


class Response(BaseContent):
    student = models.ForeignKey('Student')

    @staticmethod
    def create_response(student, res):
        self = Response.objects.create(student=student)
        for (q, a) in res.items():
            self.answer_set.create(question_id=q, answer=a)
        return {i.question.id: {
            'correct': i.question.answer,
            'your': i.answer,
            'status': i.status(),
        } for i in self.answer_set.all()}


class Answer(BaseContent):
    response = models.ForeignKey('Response')
    question = models.ForeignKey('Question', related_name='+')
    answer = models.CharField(choices=Question.ANSWER_CHOICES,
                              blank=True, null=True, max_length=10)

    def status(self):
        if not self.answer:
            return 'skip'
        elif self.question.answer == self.answer:
            return 'correct'
        else:
            return 'wrong'


class Teacher(BaseContent):
    name = models.CharField(max_length=100)
    state = models.ForeignKey('masterdata.State')
    district = models.ForeignKey('masterdata.District')
    gender = models.CharField(
        choices=GENDER_CHOICES, max_length=10)
    mobile_number = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    photo = models.ImageField(upload_to=get_upload_to('teacher', 'photo'),
                              null=True,
                              verbose_name="Upload recent photo (below 500 kb)")
    address = models.TextField()
    skill_development_sector = models.ForeignKey('SkillDevelopmentSector')
    job_role = models.ForeignKey('JobRole')
    teacher_code = models.CharField(max_length=100)
    teaching_courses = models.CharField(max_length=100)
    intrested_for_rpl_sectors = models.ManyToManyField(
        'SkillDevelopmentSector', related_name='+')
    rpl_tot_id = models.CharField(max_length=100)
    tot_batch_date = models.DateField(null=True)
    pan_card_number = models.CharField(max_length=100)
    aadhaar_card_number = models.CharField(max_length=100)
    bank_account_number = models.CharField(max_length=100)
    bank_name = models.CharField(max_length=100)
    ifsc_code = models.CharField(max_length=100)
