# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2018-09-13 11:08
from __future__ import unicode_literals

import course.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0033_auto_20180913_1626'),
    ]

    operations = [
        migrations.AddField(
            model_name='student',
            name='pf_status',
            field=models.CharField(choices=[('0', 'Result pending'), ('1', 'Passed'), ('2', 'Failed')], default='0', max_length=10),
        ),
        migrations.AlterField(
            model_name='student',
            name='adhaar_image',
            field=models.ImageField(null=True, upload_to=course.models.func, verbose_name='Upload aadhar proof (below 500 kb)'),
        ),
        migrations.AlterField(
            model_name='student',
            name='edu_cert_image',
            field=models.ImageField(null=True, upload_to=course.models.func, verbose_name='Upload latest education certificate (below 500 kb)'),
        ),
        migrations.AlterField(
            model_name='student',
            name='image',
            field=models.ImageField(null=True, upload_to=course.models.func, verbose_name='Upload recent photo (below 500 kb)'),
        ),
        migrations.AlterField(
            model_name='teacher',
            name='photo',
            field=models.ImageField(null=True, upload_to=course.models.func, verbose_name='Upload recent photo (below 500 kb)'),
        ),
    ]
