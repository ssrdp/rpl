# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2018-08-22 07:09
from __future__ import unicode_literals

import course.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0029_auto_20180822_1221'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='jobrole',
            name='sl_no',
        ),
        migrations.RemoveField(
            model_name='quota',
            name='sl_no',
        ),
        migrations.RemoveField(
            model_name='skilldevelopmentsector',
            name='sl_no',
        ),
        migrations.AlterField(
            model_name='student',
            name='adhaar_image',
            field=models.ImageField(null=True, upload_to=course.models.func, verbose_name='Upload aadhar proof (below 500 kb)'),
        ),
        migrations.AlterField(
            model_name='student',
            name='edu_cert_image',
            field=models.ImageField(null=True, upload_to=course.models.func, verbose_name='Upload latest education certificate (below 500 kb)'),
        ),
        migrations.AlterField(
            model_name='student',
            name='image',
            field=models.ImageField(null=True, upload_to=course.models.func, verbose_name='Upload recent photo (below 500 kb)'),
        ),
        migrations.AlterField(
            model_name='teacher',
            name='photo',
            field=models.ImageField(null=True, upload_to=course.models.func, verbose_name='Upload recent photo (below 500 kb)'),
        ),
    ]
