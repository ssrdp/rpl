from django.conf.urls import url
from . import views


app_name = 'course'


urlpatterns = [
    url(r'^(?P<model_slug>(:?Student))/add/$', views.StudentAdd.as_view()),
    url(r'^(?P<model_slug>(:?form3))/(?P<pk>\d+)/$', views.Form3.as_view()),
    url(r'^(?P<model_slug>(:?Teacher))/add/$', views.TeacherAdd.as_view()),
    url(r'^take-test/$', views.test),
    url(r'^as_pdf/(?P<pk>\d+)/$', views.download_pdf),
]
