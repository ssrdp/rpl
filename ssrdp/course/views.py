from collections import Counter
from django.http import JsonResponse
from crud.views import Create, Update
from .models import Student, Question, Response, Batch
from django.shortcuts import render
import random
import datetime
from django import forms


class StudentAdd(Create):
    def get_success_url(self):
        return '../../take-test/?student_id=%s'%self.object.id
    def form_valid(self, form):
        res = super(StudentAdd, self).form_valid(form)
        self.object.application_status = '1'
        self.object.save()
        return res

    def get_context_data(self, **kwargs):
        context = super(StudentAdd, self).get_context_data(**kwargs)
        context['overtable_template'] = 'course/progress-bar.html'
        context['progress'] = 20
        context['step'] = 1
        return context


class Form3(Update):
    def get_context_data(self, **kwargs):
        context = super(Form3, self).get_context_data(**kwargs)
        context['overtable_template'] = 'course/progress-bar.html'
        context['progress'] = 90
        context['step'] = 3
        return context

    def get_success_url(self):
        return '/'

    def form_valid(self, form):
        if not form.cleaned_data.get('acceptance'):
            form.add_error('acceptance', forms.ValidationError(
                'Please accept the terms and conditions to proceed'))
            return self.form_invalid(form)
        return super(Form3, self).form_valid(form)


class TeacherAdd(Create):
    def get_success_url(self):
        return '/?alert=Teacher added successfully'


def test(request):
    student = Student.objects.get(id=request.GET.get('student_id'))
    if request.is_ajax():
        qids = [i.strip('response[').strip(']')
                for i in request.GET
                if 'response[' in i]
        res = {i: request.GET.get('response[%s]'%i)
               for i in qids}
        evaluation = Response.create_response(student=student, res=res)
        marks = {'correct': 0, 'wrong': 0, 'skip': 0}
        marks.update(Counter([i['status'] for i in evaluation.values()]))
        return JsonResponse({
            'evaluation': evaluation,
            'marks': marks,
            'pass': marks.get('correct', 0) >= 7,
        })
    job_role = student.job_role
    questions = random.sample(list(job_role.question_set.all()), k=10)
    return render(request, 'course/test.html', locals())


def download_pdf(request, pk):
    student = Student.objects.get(id=pk)
    res = dict(student.__dict__)
    res['student'] = student
    return render(request, 'course/download_pdf.html', res)
