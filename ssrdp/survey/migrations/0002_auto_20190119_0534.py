# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-01-19 05:34
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('survey', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='nadicamp',
            name='address',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='nadicamp',
            name='gender',
            field=models.CharField(choices=[('m', 'Male'), ('f', 'Female'), ('t', 'Transgender')], max_length=100),
        ),
        migrations.AlterField(
            model_name='survey',
            name='address',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='survey',
            name='gender',
            field=models.CharField(choices=[('m', 'Male'), ('f', 'Female'), ('t', 'Transgender')], max_length=100),
        ),
        migrations.AlterField(
            model_name='yogashivir',
            name='address',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='yogashivir',
            name='gender',
            field=models.CharField(choices=[('m', 'Male'), ('f', 'Female'), ('t', 'Transgender')], max_length=100),
        ),
    ]
