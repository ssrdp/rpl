# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from masterdata.models import BaseContent


class SurveyBase(BaseContent):
    class Meta:
        abstract = True

    GENDER_CHOICES = (
        ('m', 'Male'),
        ('f', 'Female'),
        ('t', 'Transgender'),
    )
    MS_CHOICES = (
        ('s', 'Single'),
        ('m', 'Married'),
        ('d', 'Divorced'),
        ('w', 'Widowed'),
    )

    full_name = models.CharField(max_length=100)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    adhar_number = models.CharField(max_length=100)
    email_id = models.CharField(max_length=100)
    martial_status = models.CharField(max_length=100, choices=MS_CHOICES)
    dob = models.CharField(max_length=100, verbose_name='Date of birth')
    gender = models.CharField(max_length=100, choices=GENDER_CHOICES)
    phone_number = models.CharField(max_length=100)
    first_name_g = models.CharField(max_length=100, verbose_name='First name of Father/Guardian')
    last_name_g = models.CharField(max_length=100, verbose_name='Last name of Father/Guardian')
    religion = models.CharField(max_length=100)
    address = models.TextField()
    education_level = models.CharField(max_length=100)
    batch_start_date = models.DateField()
    batch_end_date = models.DateField()

    @staticmethod
    def filter_queryset_for_user(qs, user):
        if user.role == 'admin':
            return qs
        elif user.role == 'co':
            uids = list(user.coordinator.participant_set.values_list('user', flat=True)) + [user.id]
            return qs.filter(created_by__in=uids)
        elif user.role == 'p':
            return qs.filter(created_by=user)


class Survey(SurveyBase):
    pass


class YogaShivir(SurveyBase):
    pass


class NadiCamp(SurveyBase):
    pass


FIELDS = 'full_name first_name last_name aadhar_number email_id martial_status dob gender phone_number first_name_g last_name_g religion address education_level batch_start_date batch_end_date'
