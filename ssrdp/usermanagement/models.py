from __future__ import unicode_literals
from django.db import models
from masterdata.models import BaseContent
from django.contrib.auth.models import AbstractUser


class UserProfile(AbstractUser, BaseContent):
    ROLE_CHOICES = (
        ('p', 'Participant'),
        ('co', 'Coordinator'),
        ('admin', 'Admin'),
    )
    role = models.CharField(choices=ROLE_CHOICES, max_length=100)
    email = models.EmailField(max_length=32)
    
    def __str__(self):
        return self.username

    def save(self, *args, **kwargs):
        self.is_staff = True
        return super(UserProfile, self).save(*args, **kwargs)


class BaseUserType(BaseContent):
    user = models.OneToOneField('UserProfile', null=True)
    district = models.ManyToManyField('masterdata.District')

    username = models.CharField(max_length=100, unique=True)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.EmailField(max_length=32)
    password = models.CharField(max_length=100)

    job_role = models.ManyToManyField('course.JobRole')

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        flag = not self.pk
        super(BaseUserType, self).save(*args, **kwargs)
        if flag:
            self.user = UserProfile.objects.create_user(
                username = self.username,
                first_name = self.first_name,
                last_name = self.last_name,
                email = self.email,
                password = self.password,
                role = self.ROLE,
            )
            self.user.set_password(self.password)
            self.save()
        else:
            self.user.username = self.username
            self.user.first_name = self.first_name
            self.user.last_name = self.last_name
            self.user.email = self.email
            self.user.password = self.password
            self.user.set_password(self.password)
            self.user.save()
        return self

    def job_roles(self):
        roles = self.job_role.all()
        return ', '.join(str(i) for i in roles)

    def sectors(self):
        roles = self.job_role.all()
        sectors = set(i.skill_development_sector for i in roles)
        return ', '.join(str(i) for i in sectors)

    def districts(self):
        return ', '.join(str(i) for i in self.district.all())

    def __str__(self):
        return '%s %s (%s)' % (self.first_name, self.last_name, self.username)


class Coordinator(BaseUserType):
    ROLE = 'co'


class Participant(BaseUserType):
    coordinator = models.ForeignKey('Coordinator', blank=True, null=True)
    ROLE = 'p'
    aadhar_number = models.CharField(max_length=100)
    adhaar_image = models.ImageField(null=True)


class PanchayathRepresentative(BaseContent):
    state = models.ForeignKey('masterdata.State')
    district = models.ForeignKey('masterdata.District')
    taluk = models.ForeignKey('masterdata.Taluk')
    village = models.ForeignKey('masterdata.Village')

    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    mobile = models.CharField(max_length=100)
    email = models.EmailField(max_length=32)
    adhaar_card = models.ImageField(null=True)
    date_of_birth = models.DateField()

    @staticmethod
    def filter_queryset_for_user(qs, user):
        if user.role == 'admin':
            return qs
        elif user.role == 'p':
            return qs.filter(created_by=user)
