from django.conf.urls import url
from usermanagement import views


app_name = 'usermanagement'


urlpatterns = [
    url(r'^logout/$', views.logout_view),
    url(r'^(?P<model_slug>[^/]*)/$', views.List.as_view()),
    url(r'^(?P<model_slug>[^/]*)/add/$', views.AddCoordinator.as_view()),
    url(r'^(?P<model_slug>[^/]*)/(?P<pk>\d+)/$', views.UpdateCoordinator.as_view()),
]
