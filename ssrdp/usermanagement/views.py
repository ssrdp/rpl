from django.contrib.auth import logout
from django.shortcuts import redirect
from crud.views import Create, Update, List
from .models import Coordinator
from course.models import JobRole
from masterdata.models import District


def logout_view(request):
    logout(request)
    return redirect('/')


class CoordinatorMixin(object):
    template_name = 'usermanagement/add-coordinator.html'

    def get_context_data(self, **kwargs):
        context = super(CoordinatorMixin, self).get_context_data(**kwargs)
        context['job_roles'] = JobRole.objects.all()
        context['districts'] = District.objects.all()
        if self.request.method == 'POST':
            context['job_role_ids'] = [int(i) for i in self.request.POST.getlist('job_role')]
            context['district_ids'] = [int(i) for i in self.request.POST.getlist('district')]
        else:
            try:
                instance = self.get_object()
            except:
                pass
            else:
                context['job_role_ids'] = [i.id for i in instance.job_role.all()]
                context['districts_ids'] = [i.id for i in instance.district.all()]
        return context


class AddCoordinator(CoordinatorMixin, Create):
    pass


class UpdateCoordinator(CoordinatorMixin, Update):
    pass
