from django.conf.urls import url, include
from django.contrib import admin
from crud import urls as crudurls
from usermanagement import urls as userurls
from course import urls as courseurls
from django.http import HttpResponseRedirect
from django.conf.urls.static import static
from . import settings


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'.*css/bootstrap-cerulean.min.css$',
        lambda request: HttpResponseRedirect(
            '/static/charisma/css/bootstrap-cerulean.min.css')),
    url(r'^user/', include(userurls)),
    url(r'^course/', include(courseurls)),
    url(r'^', include(crudurls)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
